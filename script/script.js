//ローディング非表示
$(window).on('load', function() {
  
  var urlHash = location.hash;
  if(urlHash == '#Top') {
    $('#bgMovie01')[0].play();
  }
  
  $('.loading').fadeOut();
});

// IFrame Player API の読み込み
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 各プレーヤーの格納
var ytPlayer = [],
		ytWidth = '90%',
		ytHeight = 'auto',
		ytData = [
			{
      //CM
			id: 'xxxxxxxxxxx',
			area: 'modalMovie'
			}, {
      //メイキング
			id: 'xxxxxxxxxx',
			area: 'modalMovie02'
			}, {
      //インタビュー
			id: 'xxxxxxxxxx',
			area: 'modalMovie03'
			}
		];

function onYouTubeIframeAPIReady() {
	for(var i = 0; i < ytData.length; i++) {
		ytPlayer[i] = new YT.Player(ytData[i]['area'], {
				width: ytWidth,
				height: ytHeight,
				videoId: ytData[i]['id'],
				playerVars: {
					start: 0,
					rel: 0,
					showinfo: 0
				}
		});
	}
}


var getDevice = (function(){
  var ua = navigator.userAgent;
  if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
      return 'sp';
  }else if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
      return 'tab';
  }else{
      return 'other';
  }
})();

$(function(){
  //メニュー表示設定
	$('.menuBtn').on('click',function(){
		$('.menu,.menuBtn').toggleClass('on');
		$('.menu').fadeToggle();
	});
  
  
  //SP時プロフィール表示設定
	$('.profileBtn').on('click',function(){
		$('.profileBtn,.profileWrap').toggleClass('on');
		$('.profileWrap').fadeToggle();
		$('.profileWrap .closeBtn').fadeIn();
	});
	$('.box5 .txt_area .profileWrap .closeBtn').on('click',function(){
		$('.profileBtn,.profileWrap').removeClass('on');
		$('.profileWrap,.profileWrap .closeBtn').fadeOut();
	});
	
  
	//動画再生時の制御
	var $movieBtn = $('.movieBtnCmn,.movieBtnCmn + .ico'),
			$modalArea = $('.modal'),
			$modalCloseBtn = $('.modal .closeBtn');
	
  
	//モーダルオープン
	$movieBtn.on('click',function(){
		var movieNumber = $(this).attr('data-movie-number');
		$('.modalNumber'+movieNumber).addClass('on').fadeIn();
		
		$('#bgMovie01')[0].pause();
		$('#bgMovie02')[0].pause();
		
		var currentTime = ytPlayer[movieNumber].getCurrentTime();
		ytPlayer[movieNumber].seekTo(currentTime - 300).playVideo();
	});
  
	
	//モーダルクローズ
	$modalCloseBtn.on('click',function(){
		var modalNumber = $(this).parents('.modal').attr('data-modal-number');
		ytPlayer[modalNumber].pauseVideo();
		
		$modalArea.removeClass('on').fadeOut();
		
		var urlHash = location.hash;
		
		if(urlHash == '#Top') {
			$('#bgMovie01')[0].play();
		} else if(urlHash == '#CM-Making') {
			$('#bgMovie02')[0].play();
		}
		
	});
  
  
  if( getDevice == 'sp' || getDevice == 'tab' ){
  } else if( getDevice == 'other' ) {
    $('#bgMovie01').append('<source src="movie/cm_movie.mp4"><source src="movie/cm_movie.webm"><img src="./img/bg_top_sp.jpg" alt="xxxxxxxxxxx">');
    $('#bgMovie02').append('<source src="movie/making_movie.mp4"><source src="movie/making_movie.webm"><img src="./img/bg_making_sp.jpg" alt="xxxxxxxxx">');
  }
});

function menuClose(){
	$('.menu,.menuBtn').removeClass('on');
	$('.menu').fadeOut();
}




//スマホ用高さ調節設定
var vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);

var windowWidth = $(window).width();
var windowSm = 768;

if (windowWidth <= windowSm) {
  var scrollbarsVal = true;
} else {
  var scrollbarsVal = false;
}

var current;
$.scrollify({
  section:".box",
  setHeights:false,
  scrollbars:scrollbarsVal,
  before:function(i){
    current = i;
    $('.pagenation .active').removeClass('active');
    $('.pagenation').find('li').eq(i).addClass('active'); 
  },              
  afterRender:function(){
    //ページネーションの制御
    var pagenation = '<ul class="pagenation">';
    $('.box').each(function(i){
      var secName = $(this).attr('data-section-name');
      pagenation += '<li><span>'+ secName +'</span><a></a></li>';
    });
    pagenation += '</ul>';
    $('body').append(pagenation);
    $('.pagenation a').each(function(i){
      $(this).on('click',function(){
        $.scrollify.move(i);
      });
    });
    $('.pagenation li:first-child').addClass('active');

    //各パネル内Scrollボタンの制御
    $('.box .nextSection').each(function(i){
      $(this).on('click',function(){
        $.scrollify.next();
      });
    });

    //Menu内リンククリック時の制御
    $('.menuList li').each(function(i){
      $(this).on('click',function(){
        $.scrollify.move(i);
        menuClose();
      });
    });
  },
  after:function(){
    var movieBtnWrap = $('.movieBtnWrap'),
        modalOpenCheck = $('.modal').hasClass('on');

    //Topのパネルが表示されている時の背景の動画とCM動画を見るボタンの制御
    if($('.pagenation li:first-child').hasClass('active')){
      if(modalOpenCheck == true ){
        movieBtnWrap.removeClass('hide');
        $('#bgMovie01')[0].pause();
      } else {
        movieBtnWrap.removeClass('hide');
        $('#bgMovie01')[0].play();
      }
    }else{
      movieBtnWrap.addClass('hide');
      $('#bgMovie01')[0].pause();
    }

    //CMMakingのパネルが表示されている時の背景の動画の制御
    if($('.pagenation li:nth-child(3)').hasClass('active')){
      if(modalOpenCheck == true ){
        $('#bgMovie02')[0].pause();
      } else {
        $('#bgMovie02')[0].play();
      }
    }else{
      $('#bgMovie02')[0].pause();
    }

    //Linkのパネルが表示されている時CM動画を見るボタンの制御
    if($('.pagenation li:last-child').hasClass('active')){
      movieBtnWrap.addClass('allHide');
    }else{
      movieBtnWrap.removeClass('allHide');
    }
  }
});

$(window).on('resize',function(){
	var vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
  
	if(current){
		var currentScrl = $('.box').eq(current).offset().top;
		$(window).scrollTop(currentScrl);
	}
  
  var windowWidth2 = $(window).width();
  var windowSm2 = 768;

  if (windowWidth2 <= windowSm2) {
    $.scrollify.setOptions({
      scrollbars:true
    })
    $('.profileWrap,.profileWrap .closeBtn').hide();
  } else {
    $.scrollify.setOptions({
      scrollbars:false
    })
    $('.profileWrap').show();
    $('.profileWrap .closeBtn').hide();
  }
  
});


